#The goal of this programme is to have an image recognition model using CNN. The first part is
#dedicated to a simple problem, hand writing number, the second part is dedicated to image recognition.
#This code is in R language.

library(Matrix)
library(processx)
library(devtools)
devtools::install_github("rstudio/keras")
library(keras)
install_keras(method = "conda")

#Part one

#mnist_train is the training set and mnist_test the test set
mnist_train=read.csv(file.choose())
mnist_test=read.csv(file.choose())

#Same separation for labels
label_train=mnist_train$label
label_test=mnist_test$label

#To prepare this data for training I one-hot encode the vectors into binary 
#class matrices using the Keras to_categorical() function:
label_train <- to_categorical(label_train, 10)
label_test <- to_categorical(label_test, 10)

#I also change the classe of the data sets
image_train=as.matrix(mnist_train[,2:785])
image_test=as.matrix(mnist_test[,2:785])

# rescale between 0 and 1
image_train <- image_train/255
image_test <- image_test/255

#Here you can plot the k th image:
k=9
imag=matrix(nrow = 28, ncol = 28)
for (i in 0:27){
  for (j in 0:27){
    imag[27-i,27-j]=image_train[k,27-i+j*28]
  }
}
image(imag)

#I creat a sequential model and then add layers using the pipe (%>%) operator:
model <- keras_model_sequential() 
model %>% 
  layer_dense(units = 256, activation = 'relu', input_shape = c(784)) %>% 
  layer_dropout(rate = 0.4) %>% 
  layer_dense(units = 128, activation = 'relu') %>%
  layer_dropout(rate = 0.3) %>%
  layer_dense(units = 10, activation = 'softmax')

#Next, compile the model with appropriate loss function, optimizer, and metrics:
model %>% compile(
  loss = 'categorical_crossentropy',
  optimizer = optimizer_rmsprop(),
  metrics = c('accuracy')
)

#Use the fit() function to train the model for 30 epochs
history <- model %>% fit(
  image_train, label_train, 
  epochs = 30, batch_size = 128, 
  validation_split = 0.2
)

#Evaluate the model's performance on the test data:
model %>% evaluate(image_test,label_test)

#Generate predictions on new data:
pretiction=model %>% predict_classes(image_test)

# We can see where the recongition succed:
pretiction==mnist_test$label 

#Part 2
#Here I implement an animal recogntion model. There are tree type, bird, frog and cat

library(png)

#The 32x32 images are flattened into length 1024 vectors
imag=data.frame()
for (i in 1:450){
  imag=rbind(imag,array_reshape(readPNG(sprintf("c:/Users/larry/Downloads/cifar10-images/%d.png",i)),c(1, 1024)))
}

label=read.csv(file.choose()) #Classe of each image

#The train and test sample 
idxs=sample(1:450,as.integer(0.9*450))
train_imag_cifar <- as.matrix(imag[idxs,])
test_imag_cifar <- as.matrix(imag[-idxs,])

train_label_cifar=as.numeric(label[idxs,][,2])-1
test_label_cifar=as.numeric(label[-idxs,][,2])-1

#As in the previous part:
train_label_cifar <- to_categorical(train_label_cifar, 3)
test_label_cifar <- to_categorical(test_label_cifar, 3)

#I creat a sequential model and then add layers using the pipe (%>%) operator:
model_cifar <- keras_model_sequential() 
model_cifar %>% 
  layer_dense(units = 400, activation = 'relu', input_shape = c(1024)) %>% 
  layer_dropout(rate = 0.4) %>% 
  layer_dense(units = 200, activation = 'relu') %>%
  layer_dropout(rate = 0.3) %>%
  layer_dense(units = 3, activation = 'softmax')

#Next, compile the model with appropriate loss function, optimizer, and metrics:
model_cifar %>% compile(
  loss = 'categorical_crossentropy',
  optimizer = optimizer_rmsprop(),
  metrics = c('accuracy')
)

#Train
history_cifar <- model_cifar %>% fit(
  train_imag_cifar, train_label_cifar, 
  epochs = 30, batch_size = 128, 
  validation_split = 0.2
)

model_cifar %>% evaluate(test_imag_cifar,test_label_cifar) 
# As we can see the accuracy around 0.422 is just a bit better than the random case 0.3333

pretiction_cifar=model_cifar %>% predict_classes(test_imag_cifar)

# We can see where the recognition succed:
pretiction_cifar==as.numeric(label[-idxs,][,2])-1
